#!/bin/bash

## every minute tasks
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/cpu.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/cpu.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/memory.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/memory.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/uptime.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/uptime.txt

tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/nginx.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/nginx.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/apache2.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/apache2.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/plexmediaserver.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/plexmediaserver.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/syncthing.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/syncthing.txt
tail -n 1440 /var/www/ant.lgbt/dash/_data/AxxivServer/jellyfin.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/jellyfin.txt

## 15mins
tail -n 96 /var/www/ant.lgbt/dash/_data/AxxivServer/storage_root.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/storage_root.txt
tail -n 96 /var/www/ant.lgbt/dash/_data/AxxivServer/storage_data.txt > /var/www/ant.lgbt/dash/_data/AxxivServer/storage_data.txt
