function update() {
    const servers = ["AxxivServer"];
    servers.forEach(updateServer);
}

function updateServer(item) {
    updateUptime(item);
    updateCpu(item);
    updateMemory(item);
    updateStorage(item); 
    
    const services = ["Nginx", "Jellyfin", "PlexMediaServer", "Syncthing", "Apache2", "Postfix", "Dovecot"];
    
    for (var i = 0; i < services.length; i++) {
	updateService(item, services[i]);
    }
}

function updateUptime(server) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() { 
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("AxxivServerUptime").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET", "read.php?file=_data/" + server + "/uptime.txt", true);
    xmlhttp.send();
}

function updateCpu(server) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() { 
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText > 90) {
                document.getElementById("AxxivServerCpu").className = "veryhighstat";
            } else if (this.responseText > 70) {
                document.getElementById("AxxivServerCpu").className = "highstat";
            } else {
                document.getElementById("AxxivServerCpu").className = "lowstat";
            }
            document.getElementById("AxxivServerCpu").value = this.responseText;
            
        }
    };
    xmlhttp.open("GET", "read.php?file=_data/" + server + "/cpu.txt", true);
    xmlhttp.send();
}

function updateMemory(server) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() { 
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText > 90) {
                document.getElementById("AxxivServerMemory").className = "veryhighstat";
            } else if (this.responseText > 70) {
                document.getElementById("AxxivServerMemory").className = "highstat";
            } else {
                document.getElementById("AxxivServerMemory").className = "lowstat";
            }
            document.getElementById("AxxivServerMemory").value = this.responseText;
            
        }
    };
    xmlhttp.open("GET", "read.php?file=_data/" + server + "/memory.txt", true);
    xmlhttp.send();
}

function updateStorage(server) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() { 
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText > 90) {
                document.getElementById("AxxivServerStorage").className = "veryhighstat";
            } else if (this.responseText > 70) {
                document.getElementById("AxxivServerStorage").className = "highstat";
            } else {
                document.getElementById("AxxivServerStorage").className = "lowstat";
            }
            document.getElementById("AxxivServerStorage").value = this.responseText;
            
        }
    };
    xmlhttp.open("GET", "read.php?file=_data/" + server + "/storage_data.txt", true);
    xmlhttp.send();
}

function updateService(server, service) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() { 
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText > 0) {
                document.getElementById(server + service).className = "entryServiceRunning";
            } else {
                document.getElementById(server + service).className = "entryServiceStopped";
            }
        }
    };
    xmlhttp.open("GET", "read.php?file=_data/" + server + "/" + service.toLowerCase() + ".txt", true);
    xmlhttp.send();
}


update();
setInterval(() => {
    update();
}, 30000);
